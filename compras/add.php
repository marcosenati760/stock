<?php
require_once '../header.php'; 
require_once('../conexion.php');

$sql ='SELECT id,descripcion FROM productos;';
$stmt = $pdo->prepare($sql);
$stmt ->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

$sqlc ='SELECT MAX(id) AS id FROM compras;';
$stmtc = $pdo->prepare($sqlc);
$stmtc ->execute();
$compra_id = $stmtc->fetchAll(PDO::FETCH_ASSOC);
$compra_id = $compra_id[0]['id'] + 1;
?>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <h3>Compras</h3>
        <table class="table table-bordered table-responsive">    
            <form method="post" action="add.php"> 
            <tr><td><b>ID</td><td><input type="text" name="id" value="<?=$compra_id?>" readonly></td></tr>
            <tr><td><b>Producto</td>
            <td>
            <select name="producto_id" id="producto_id">
                <option value="" selected>Selecione</option>
                <?php foreach($data as $row) : ?>
                    <option value="<?= $row['id']; ?>"><?= $row['descripcion']; ?></option>
                <?php endforeach ?>
            </select>
            </td>
            </tr>
            <tr><td><b>cantidad</td><td><input type="text" name="cantidad"></td></tr>
            <tr><td><b>Precio</td><td><input type="text" name="precio"></td></tr>
            <tr><td><b>Data</td><td><input type="text" name="data"></td></tr>
            <tr>
                <td></td><td><input class="btn btn-primary" name="enviar" type="submit" value="Registrar">&nbsp;&nbsp;&nbsp;
                    <input class="btn btn-warning" name="enviar" type="button" onclick="location='index.php'" value="Regresar">
                </td>
            </tr>
            </form>
        </table>
        </div>
    </div>
</div>

<?php
if(isset($_POST['enviar'])){
    // Receber valores digitados no form
    $producto_id = $_POST['producto_id'];
    $cantidad = $_POST['cantidad'];
    $precio = $_POST['precio'];
    $data = $_POST['data'];
    $compra_id = $_POST['id'];

    // Pegar stock_maximo de productos para $producto_id
    $sql ='SELECT stock_maximo AS maximo FROM productos WHERE id = '.$producto_id;
    $stmt = $pdo->prepare($sql);
    $stmt ->execute();
    $max = $stmt->fetch(PDO::FETCH_ASSOC);
    $max = $max['maximo'];

    // Pegar a cantidad em stock para $producto_id
    $sql ='SELECT sum(cantidad) AS cantidad FROM stock WHERE id = '.$producto_id;
    $stmt = $pdo->prepare($sql);
    $stmt ->execute();
    $soma_stock = $stmt->fetch(PDO::FETCH_ASSOC);
    $soma_stock = $soma_stock['cantidad'];
    
    if(is_null($soma_stock)) {
        $soma_stock = 0;
    }

    $quant = $max + $soma_stock;

    if($cantidad > $quant) {
    ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>  
        <script>
          //  document.getElementById("olamundo").addEventListener("click", exibeMensagem);
            Swal.fire({
              icon: 'error',
              title: 'cantidad muito alta',
              text: 'O stock máximo é de '+"<?=$max?>",
            })
        // https://blog.betrybe.com/desenvolvimento-web/sweetalert/
        </script>
    <?php
    }else{
        try{
           $sql = "INSERT INTO compras(producto_id, cantidad, precio, data) VALUES (?,?,?,?)";
           $stm = $pdo->prepare($sql)->execute([$producto_id, $cantidad, $precio, $data]);
           $quant_form = $cantidad;

           if($soma_stock == 0){
               // Adicionar a cantidad comprada ao stock
               $sqle = "INSERT INTO stock(producto_id, cantidad) VALUES (?,?)";
               $stme = $pdo->prepare($sqle)->execute([$producto_id, $cantidad]);
           }else{
               // Atualizar a cantidad comprada no stock
               $stock_atual = $quant_form + $soma_stock;

               $sqle = "UPDATE stock set cantidad = $stock_atual WHERE producto_id = ?";
               $stme = $pdo->prepare($sqle)->execute([$producto_id]);     
           }

           if($stm){
               echo 'Dados inseridos com sucesso';
         header('location: ../stock/index.php');
           }
           else{
               echo 'Erro ao inserir os dados';
           }
       }
       catch(PDOException $e){
          echo $e->getMessage();
       }
    }
}

require_once('../footer.php');
?>
